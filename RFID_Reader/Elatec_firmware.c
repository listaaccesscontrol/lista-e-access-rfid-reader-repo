// *******************************************************************************************
//
//	  File: App_SBDR100_Stanley_Balck_&_Decker_RS232_Weigand_Simple_Motor_Fire.c
//	  Date: 2018/07/02
//	  Version: 1.00
//	  Mod: AA
//	  Case: #11413
//
// *******************************************************************************************

#include "twn4.sys.h"
#include "apptools.h"
#include "prs.h"

const unsigned char AppManifest[] = { EXECUTE_APP, 1, EXECUTE_APP_ALWAYS, TLV_END };

#define MAXIDBYTES  8
#define MAXIDBITS   (MAXIDBYTES*8)
#define MAXCMDLEN	 8

bool HostTestCmd(void);

byte ID[MAXIDBYTES];
int IDBitCnt;
int TagType;

byte LastID[MAXIDBYTES];
int LastIDBitCnt;
int LastTagType;
int LastTagTypeStored;
bool RS232ReqAvailable;

byte DataID[MAXIDBYTES+2]; 
int DataBitCnt;

byte Cmd[MAXCMDLEN];
int CmdLen;
bool CmdReady;

// ******************************************************************
// ****** Section containing configurable behaviour *****************
// ******************************************************************

// If necessary adjust default parameters
#ifndef __APP_CONFIG_H__
    #define LFTAGTYPES        		TAGMASK(LFTAG_HIDPROX)
    #define HFTAGTYPES        		(TAGMASK(HFTAG_MIFARE) | TAGMASK(HFTAG_HIDICLASS) )
    #define BIT_LENGTHS       		0xFFFFFFFFFFFFFFFF
    #define ICLASS_READMODE_VALUE  	ICLASS_READMODE_UID
#endif

#define DATA0 			GPIO5 	// use GPIO2 for DATA0
#define DATA1 			GPIO6	// use GPIO3 for DATA1
#define CARD_PRESENT	GPIO4


#define SLEEP_MS		10000
#define SLEEP_FLAGS		SLEEPMODE_SLEEP | WAKEUP_BY_LPCD_MSK | WAKEUP_BY_USB_MSK | WAKEUP_BY_TIMEOUT_MSK |WAKEUP_BY_COM1_MSK


// Adjust parameters as configured in AppBlaster
const byte TLVBytes[] = { ICLASS_READMODE, 1, ICLASS_READMODE_VALUE, TLV_END };
void ConfigSetParameters(void)
{
	SetParameters(TLVBytes,sizeof(TLVBytes));
}

// Setup tag types as configured in AppBlaster
void ConfigSetTagTypes(void)
{
	if (LFTAGTYPES || HFTAGTYPES)
    	SetTagTypes(LFTAGTYPES,HFTAGTYPES);
}

// Test: Is ID length according to configured in AppBlaster?
bool ConfigTestIDLength(void)
{
	unsigned long long BitMask = 1ULL << (IDBitCnt-1);
	return (BitMask & BIT_LENGTHS) != 0;
}

// ******************************************************************

int main(void)
{
	// COM1 is running @ 9600 Baud 8N1 by default. The code below shows how to
	// modify default baud rate/parity/stop bits.
	
	TCOMParameters COMParameters;
	//COMParameters.BaudRate = 115200;
	COMParameters.BaudRate = 9600;
  	COMParameters.WordLength = COM_WORDLENGTH_8;
	COMParameters.Parity = COM_PARITY_NONE;
  	COMParameters.StopBits = COM_STOPBITS_1;
	COMParameters.FlowControl = COM_FLOWCONTROL_NONE;
	SetCOMParameters(CHANNEL_COM1,&COMParameters);
	
	
	//I2CInit(I2CMODE_SLAVE | I2C_ADDRESS | I2CMODE_CHANNEL);
	//int HostChannel = GetHostChannel();
    //int HostChannel = CHANNEL_I2C;
	int HostChannel = CHANNEL_COM1;
	//int HostChannel = CHANNEL_USB;
	// Set Host Channel
	SetHostChannel(HostChannel);
	//SimpleProtoInit(HostChannel,PRS_COMM_MODE_ASCII | PRS_COMM_CRC_OFF);
	
	//setup GPIO tags
	GPIOConfigureOutputs(CARD_PRESENT, GPIO_PUPD_NOPULL, GPIO_OTYPE_PUSHPULL);
	//GPIOConfigureOutputs(DATA0, GPIO_PUPD_NOPULL, GPIO_OTYPE_OPENDRAIN);
	GPIOClearBits(CARD_PRESENT);
	//GPIOClearBits(DATA0);
	
    // Show the startup message
    if (GetHostChannel() == CHANNEL_USB)
    {
        // A V24 device is writing the version at startup
        HostWriteVersion();
		HostWriteChar('\n');
        HostWriteChar('\r');
    }
	
	// Adjust parameters   
	ConfigSetParameters();
	// Set tag types
	ConfigSetTagTypes();
    	
    // Init LEDs
    LEDInit(REDLED | GREENLED);
    // Turn on green LED
    LEDOn(GREENLED);
    // Turn off red LED
    LEDOff(REDLED);
    // Make some noise at startup at low volume
    SetVolume(30);
    BeepLow();
    BeepHigh();
    // Continue with maximum  volume
    SetVolume(100);
	
	LEDOff(GREENLED);
    // Init Section:
	// Use GPIO2 and GPIO3 for Wiegand interface
	//GPIOConfigureOutputs(DATA0 | DATA1,GPIO_PUPD_PULLUP,GPIO_OTYPE_PUSHPULL);
	// Enter idle level. In this case we have active low outputs
	//GPIOSetBits(DATA0 | DATA1);
	
    // No transponder found up to now
    LastTagType = NOTAG;
	RS232ReqAvailable = false;
	CmdReady = true;
	
	StartTimer(2000);
	
    while (true)
    {
		
		
        // Search a transponder
        if (SearchTag(&TagType,&IDBitCnt,ID,sizeof(ID)))
        {
            if (ConfigTestIDLength())
            {
                // Is this transponder new to us?
                if (TagType != LastTagType || IDBitCnt != LastIDBitCnt || !CompBits(ID,0,LastID,0,MAXIDBITS))
                {
                    // Save this as known ID, before modifying the ID for proper output format
                    CopyBits(LastID,0,ID,0,MAXIDBITS);
                    LastIDBitCnt = IDBitCnt;
                    LastTagType = TagType;
                    RS232ReqAvailable = true;

                    // Yes! Sound a beep
                    BeepHigh();
                    // Turn off the green LED
                    LEDOff(GREENLED);
                    // Let the red one blink, start with on-state
                    LEDOn(REDLED);
                    LEDBlink(REDLED,25,500);
					
					// Send data over GPIO2 and GPIO3 using Wiegand interface
					//SendWiegand(DATA0,DATA1,40,2000,ID,IDBitCnt);
					//SendWiegand(DATA0,DATA1,125,2000,ID,IDBitCnt);
					GPIOClearBits(CARD_PRESENT);
					GPIOSetBits(CARD_PRESENT);
					//GPIOSetBits(DATA0);
					//GPIOClearBits(CARD_PRESENT);
					
                    //HostWriteBin(ID,IDBitCnt,(IDBitCnt+7)/8*2);
                    //HostWriteChar('\n');
					//HostWriteChar('\r');
                }
                // Start a timeout of two seconds
                StartTimer(5000);
            }

        }
        if (TestTimer())
        {
            LEDOff(GREENLED);
			LEDOff(REDLED);
			LastTagType = NOTAG;
			Sleep(SLEEP_MS, SLEEP_FLAGS);
			
			StartTimer(250);
        }else{
			
		}
		/*
		if (SimpleProtoTestCommand()){

			// SimpleProtoMessage/SimpleProtoMessageLength now contains command from host
			SimpleProtoExecuteCommand();
			// SimpleProtoMessage/SimpleProtoMessageLength now contains response to host
			SimpleProtoSendResponse();
		}
		*/
		if (HostTestCmd()){
			if(CmdLen == 1){
				
				switch (Cmd[0])
				{	
					case 83:		// 'S'
						if (RS232ReqAvailable){
							GPIOClearBits(CARD_PRESENT);
							//GPIOClearBits(DATA0);
							RS232ReqAvailable = false;
							HostWriteBin(LastID,LastIDBitCnt,(LastIDBitCnt+7)/8*2);
							//HostWriteChar('\n');
							HostWriteChar('\r');
							//HostWriteString("Y\n\r");

							// Yes! Sound a beep
							BeepHigh();
							/*
							// Turn off the green LED
							LEDOff(GREENLED);
							// Let the red one blink, start with on-state
							LEDOn(REDLED);
							LEDBlink(REDLED,25,500);
							StartTimer(500);
							*/
							
						}else{
							//HostWriteString("N\r");
						}
						break;
					
					case 84:		// 'T'
						HostWriteBin(LastID,LastIDBitCnt,(LastIDBitCnt+7)/8*2);
						HostWriteChar('\r');
						break;
					case 85:		// 'U'
						HostWriteString("Comm OK");
						HostWriteChar('\r');
						break;
					default:
						HostWriteString("?\r");
						break;
				}	
			}
		}
		
    }
}

bool HostTestCmd(void){
	if (CmdReady)
	{
		CmdLen = 0;
		CmdReady = false;
	}
	while (HostTestByte())
	{
		byte Byte = HostReadByte();
		
		if (Byte == 13)
		{
			CmdReady = true;
			return true;
		}
		if (Byte != '\n')
		{
     		if (CmdLen < MAXCMDLEN){
	    		Cmd[CmdLen] = Byte;
				CmdLen++;
			}
		}
	}
	return false;
}
